# Conattest
progress directory is for previous logging of commmands
ardupilot is a submodule for current work

USE Copter-3.6.7 branch AS BASE! (Or else sim_vehicle.py won't show copter!)

setup commands:
check out initial setup from https://ardupilot.org/dev/docs/building-for-navio2-on-rpi3.html

alias waf="$PWD/modules/waf/waf-light"
CC=clang CXX=clang++ ./waf configure --board=sitl --debug --disable-tests
(default to sitl also) (only new version has --disable-scripting, 3.6.7 does not have this lua scripting)

/home/osboxes/Desktop/conattest/ardupilot/Tools/ardupilotwaf/boards.py contains the flags for compilation of diff boards

get line number of object files `find ./ -name *.o | wc -l` which is 642.
get object files to a file `find ./ -name *.o | tr '\n' ' ' >> bc_files.txt`
while libArduCopter_libs.a contains 571. `ar t ./build/sitl/lib/libArduCopter_libs.a  | wc -l` # show contents in a static lib and count lines

build the target: waf --targets bin/arducopter 
add -fuse-ld=gold -flto flags to final linking stage for .bc linking.

Cross-compilation:
sudo mount -o loop,offset=40894464 rpi3_optee_navio_41495.img /media/rpi/
40894464=512bytes ** 79872 (start sector)

CC=clang CXX=clang++ CXXFLAGS="--target=arm-linux-gnueabihf -I/media/rpi/usr/include/arm-linux-gnueabihf -I/media/rpi/usr/include/arm-linux-gnueabihf/c++/6" waf --debug --disable-tests configure --board=navio2 
(--disable-scripting) for newer versions


use llvm-ar instead of /usr/bin/arm-linux-gnueabihf-ar to create lib/libArduCopter_libs.a

build the target: waf --targets bin/arducopter 
add -fuse-ld=gold -flto flags to final linking stage for .bc linking.


# Summary on changes to waf:
1. add -fcsi -emit-llvm -flto to CXXFLAGS in c4che for compiling llvm bitcode objects/ or wscript boards.py
2. use llvm-ar to create .a static lib
3. use -fuse-ld=gold -flto /home/osboxes/Desktop/code-coverage.o /home/osboxes/Desktop/llvm/build/lib/clang/3.9.0/lib/linux/libclang_rt.csi-armhf.a to link with instrumentation, code-coverage.o (this can be cross compiled also i guess) and libclang_rt.csi-armhf.a(theoretically can be cross compiled but I am good with now) built by rpi toolchain. others can be cross compiled. 


# Analysis:
opt -dot-callgraph link_all.bc
search for ioctl can get all functions calling ioctl

# ll construction:
1. file attributes: 
source_filename = "llvm-link" target datalayout = "e-m:e-p:32:32-i64:64-v128:64:128-a:0:32-n32-S64" target triple = "armv6kz--linux-gnueabihf"
2. struct/class def: 
%class.AP_AccelCal_Client = type { i32 (...)** }
%"struct.AP_Param::GroupInfo" = type { i8, i8, i8*, i32, %union.anon.8, i16 }
3. section attributes:
$_ZN16AP_Baro_ICM20789D2Ev = comdat any
4. global var.s:
@.str.16.4051 = private unnamed_addr constant [1 x i8] zeroinitializer, align 1
@_ZN11AP_ICEngine10_singletonE = global %class.AP_ICEngine* null, align 4
5. alias:
@_ZN11OpticalFlowC1Ev = alias %class.OpticalFlow* (%class.OpticalFlow*), %class.OpticalFlow* (%class.OpticalFlow*)* 
6. func. def.:
`; Function Attrs: nounwind
define internal void @_GLOBAL__sub_I_AP_AccelCal.cpp() #0 section ".text.startup" !dbg !44658 {
entry:
  call void @__cxx_global_var_init(), !dbg !44660
  call void @__cxx_global_var_init.1(), !dbg !44661
  ret void
}`
7. attributes:
attributes #0 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="arm1176jzf-s" "target-features"="+dsp,+strict-align,+vfp2" "unsafe-fp-math"="false" "use-soft-float"="false" }
8. debug info:
!llvm.dbg.cu = !{!0, !4785, !4793, !5103, ...}
!llvm.ident = !{!44653, !44653, !44653, ...}
!llvm.module.flags = !{!44654, !44655, !44656, !44657}
!0 = distinct !DICompileUnit(language: DW_LANG_C_plus_plus, file: !1, producer: "clang version 3.9.0 ", isOptimized: false, runtimeVersion: 0, emissionKind: FullDebug, enums: !2, retainedTypes: !89, globals: !92, imports: !4449)
!1 = !DIFile(filename: "../../libraries/AP_AccelCal/AP_AccelCal.cpp", directory: "/home/osboxes/Desktop/conattest/ardupilot/build/navio2")

-Wl,option
Pass option as an option to the linker. If option contains commas, it is split into multiple options at the commas. You can use this syntax to pass an argument to the option. For example, -Wl,-Map,output.map passes -Map output.map to the linker. When using the GNU linker, you can also get the same effect with -Wl,-Map=output.map. 
pass --verbose to linker to show linker script.

show default linker script: 
ld --verbose

add function section & data section:
-ffunction-sections -fdata-sections

`osboxes@osboxes:~/Desktop/conattest/ardupilot$ llvm-nm ./build/navio2/libraries/GCS_MAVLink/
GCS_Common.cpp.4.d                     GCS_Fence.cpp.0.o                      GCS_Rally.cpp.0.d                      GCS_Signing.cpp.0.o                    MissionItemProtocol_Fence.cpp.0.o
GCS_Common.cpp.4.o                     GCS_FTP.cpp.0.d                        GCS_Rally.cpp.0.o                      include/                               MissionItemProtocol_Rally.cpp.0.d
GCS.cpp.0.d                            GCS_FTP.cpp.0.o                        GCS_serial_control.cpp.0.d             MAVLink_routing.cpp.0.d                MissionItemProtocol_Rally.cpp.0.o
GCS.cpp.0.o                            GCS_MAVLink.cpp.0.d                    GCS_serial_control.cpp.0.o             MAVLink_routing.cpp.0.o                MissionItemProtocol_Waypoints.cpp.0.d
GCS_DeviceOp.cpp.0.d                   GCS_MAVLink.cpp.0.o                    GCS_ServoRelay.cpp.0.d                 MissionItemProtocol.cpp.0.d            MissionItemProtocol_Waypoints.cpp.0.o
GCS_DeviceOp.cpp.0.o                   GCS_Param.cpp.0.d                      GCS_ServoRelay.cpp.0.o                 MissionItemProtocol.cpp.0.o            
GCS_Fence.cpp.0.d                      GCS_Param.cpp.0.o                      GCS_Signing.cpp.0.d                    MissionItemProtocol_Fence.cpp.0.d`

llvm-nm ./build/navio2/libraries/GCS_MAVLink/GCS_Common.cpp.4.o and GCS_DeviceOp.cpp.0.o and ... get all the functions defined by GCS_Mavlink library

llc translates bitcode to object file

objcopy --rename-section .text=.dflash_code a.out can be used to rename .text and .data section in object files, so we can first convert .ll or .bc to object file and then rename the sections. 

objdump -t -> get the sections' info.
objdump -t -j, --section=.rdtext bin/arducopter -> show all symbols in a specified section


go to ArduCopter/
../Tools/autotest/sim_vehicle.py --map --console -N -> run sim without build

SIM_VEHICLE: Run MavProxy
SIM_VEHICLE: "mavproxy.py" "--master" "tcp:127.0.0.1:5760" "--sitl" "127.0.0.1:5501" "--out" "127.0.0.1:14550" "--out" "127.0.0.1:14551" "--map" "--console"

RiTW: Starting ArduCopter : /home/osboxes/Desktop/conattest/ardupilot/build/sitl/bin/arducopter -S -I0 --model + --speedup 1 --defaults /home/osboxes/Desktop/conattest/ardupilot/Tools/autotest/default_params/copter.parm

show mmap:
/proc/[pid]/maps and /proc/[pid]/smaps & pmap -x [pid]

find all object files:
`find  ./ -name *.o -exec readlink -f {} \;`
llc to compile them to object:
`xargs -0 -n 1 llc -filetype=obj < <(tr \\n \\0 <objects.txt)`
count object number:
`find ./ -name *.o.o | wc -l`

#1 TODO: implement mprotect in kernel module. so we can claim use seccomp to restrict syscall of arducoptor binary.
#2 TODO: do the function name and global struct name extraction by llvm to all objects in GCM_Mavlink (think of other compartment)


alias waf="$PWD/modules/waf/waf-light"

Cross Compile sitl:

step 1:
CC=clang CXX=clang++ CXXFLAGS="--target=arm-linux-gnueabihf -I/media/rpi/usr/include/arm-linux-gnueabihf -I/media/rpi/usr/include/arm-linux-gnueabihf/c++/6" waf --debug --disable-tests configure --board=sitl 
(wscript board.py need to change /home/osboxes/Desktop/conattest/ardupilot/Tools/ardupilotwaf/boards.py to add toolchain = 'arm-linux-gnueabihf' to class sitl(Board))

waf --targets bin/arducopter
(note: need to load rpi3 image first, or else system header missing error)

step 2:
run sitl_bc_to_obj.txt to use llc to compile .bc to .o
run arm_link_objects.txt to link 
(note: need to link with /home/osboxes/Desktop/raspbian-tee/optee_client/out/export/lib/libteec.a (add to arm_linker_script_link.txt) and add -IHeaders in wscript)

step 3:
load tee by: sudo tee-supplicant &
launch arducopter by: sudo ./start_sitl_arducopter.txt (dont know why need sudo here, maybe .so used sudo permission, I am too lazy to figure out here)

on HOST: "mavproxy.py" "--master" "tcp:192.168.1.11:5760" "--sitl" "192.168.1.11:5501" "--out" "192.168.1.11:14550" "--out" "192.168.1.11:14551" "--map" "--console"

on PI3: ./arducopter -S -I0 --model + --speedup 1 --defaults ./copter.parm

mode guided
arm throttle
takeoff 40

instrumentation:
step 1:
link MAVLink library together with llvm-link (make run in gen-func in llvm-pass) (MAVLink lib objects done with gen-func Makefile)
step 2:
use the analysis result to instrument sitl (inserthook1 for extern func, inserthook2 for insert call before) (use gen_commands.py to generate the opt instrumentation commands) (note that extern "C" use C naming convention, without it, cpp will use name mangling. )

TODO:
Mavlink ret instruction not instrumented. (can see which ones are called by other lib to find them)
Other lib ret instruction not instrumented (can use call to Mavlink lib to find them)
hash in pta not implemented
4 sitl experiments
ardupilot real software instrumentation
cfi comparison - real-time tasks
cfi comparison - memory size